﻿namespace Aspects.Crosscuttings.Helpers
{
    using System.Diagnostics;
#if PCL
    using System.Reflection;
#endif

    public static class InstancePropertyNameResolver
    {
        public static object Resolve(object primaryInstance, string propertyName)
        {
            var propertyDeepNames = propertyName.Split('.');

            return Resolve(primaryInstance, propertyDeepNames);
        }

        public static object Resolve(object primaryInstance, string[] propertyDeepNames, int skip = 0)
        {
            var propertyValue = primaryInstance;

            for (int i = skip; i < propertyDeepNames.Length; i++)
            {
                propertyValue = GetPropertyValue(propertyValue, propertyDeepNames[i]);

                if (propertyValue == null)
                {
                    break;
                }
            }

            return propertyValue;
        }

        private static object GetPropertyValue(object value, string propertyName)
        {
            var type = value.GetType();
#if PCL
            var prop = type.GetRuntimeProperty(propertyName);
#else
            var prop = type.GetProperty(propertyName);
#endif

            if (prop == null)
            {
                Debug.WriteLine($"Cannot get property '{propertyName}' of type '{type}'!");
                return null;
            }

#if PCL
            return prop.GetMethod
                 .Invoke(value, new object[0]);
#else
            return prop.GetGetMethod()
                    .Invoke(value, new object[0]);
#endif
        }
    }
}
