

# Aspects Order

| Order | Name                | Definition |
|-------|---------------------|------------|
| 0     | Context             |            |
| 10    | IdentityContext     |            |
| 100   | StartTask	          |            |
| 200   | Info                |            |
| 300   | IncrementCounter    |            |
| 300   | IncrementPerSeconds |            |
| 300   | DecrementCounter    |            |
| 400   | IncrementPercent    |            |
| 400   | DecrementPercent    |            |
| 400   | WarnOnException     |            |
| 400   | WarnOnTimeExceeded  |            |
| 1000  | Debug               |            |