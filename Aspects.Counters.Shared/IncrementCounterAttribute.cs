﻿
namespace Aspects.Counters
{
    using System;
    using System.Diagnostics;

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Assembly | AttributeTargets.Module)]
    public class IncrementCounterAttribute : CounterAspect
    {
        public int IncrementBy { get; set; } = 1;
        
        public override void OnEntry()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(this.Category, this.Counter, this.CounterInstance, false))
            {
                counter.IncrementBy(this.IncrementBy);
            }
        }

        public override void OnException(Exception exception)
        {

        }

        public override void OnExit()
        {

        }
    }
}
