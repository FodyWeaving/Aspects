﻿
namespace Aspects.Counters
{
    using System;
    using System.Diagnostics;

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Assembly | AttributeTargets.Module)]
    public sealed class DecrementPercentAttribute : CounterAspect
    {
        public long Max { get; set; } = 100;

        public override void OnEntry()
        {
            using (PerformanceCounter counter = new PerformanceCounter(this.Category, this.Counter, this.CounterInstance, false))
            {
                counter.RawValue -= 100 / this.Max;
            }
        }

        public override void OnException(Exception exception)
        {

        }

        public override void OnExit()
        {
           
        }

        protected override void OnInitialized()
        {
            base.OnInitialized();

            if (this.Max == default(long))
            {
                throw new ArgumentNullException(nameof(this.Max));
            }
        }
    }
}
