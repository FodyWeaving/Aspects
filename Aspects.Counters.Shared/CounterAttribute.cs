﻿
namespace Aspects.Counters
{
    using System;
    using System.Diagnostics;

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Assembly | AttributeTargets.Module)]
    public sealed class CounterAttribute : CounterAspect
    {
        public override void OnEntry()
        {
            if(this.ArgNames.Count != 1 || !this.ArgNames.Contains("value"))
            {
                throw new InvalidOperationException("This aspect can only be applied on a property setter.");
            }

            if (!(this.ArgValues[0] is long))
            {
                return;
            }

            using (PerformanceCounter counter = new PerformanceCounter(this.Category, this.Counter, this.CounterInstance, false))
            {
                counter.RawValue = (long)this.ArgValues[0];
            }
        }

        public override void OnException(Exception exception)
        {
           
        }

        public override void OnExit()
        {
            
        }
    }
}
