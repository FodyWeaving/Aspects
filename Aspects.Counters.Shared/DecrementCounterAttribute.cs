﻿
namespace Aspects.Counters
{
    using System;
    using System.Diagnostics;

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Assembly | AttributeTargets.Module)]
    public sealed class DecrementCounterAttribute : CounterAspect
    { 
        public override void OnEntry()
        {
            using (PerformanceCounter counter = new PerformanceCounter(this.Category, this.Counter, this.CounterInstance, false))
            {
                counter.Decrement();
            }
        }

        public override void OnException(Exception exception)
        {

        }

        public override void OnExit()
        {
           
        }
    }
}
