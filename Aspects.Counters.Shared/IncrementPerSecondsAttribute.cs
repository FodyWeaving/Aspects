﻿
namespace Aspects.Counters
{
    using System;

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Assembly | AttributeTargets.Module)]
    public sealed class IncrementPerSecondsAttribute : IncrementCounterAttribute
    {
        public IncrementPerSecondsAttribute()
        {
            base.PerformanceCounterType = System.Diagnostics.PerformanceCounterType.RateOfCountsPerSecond32;
        }
    }
}
