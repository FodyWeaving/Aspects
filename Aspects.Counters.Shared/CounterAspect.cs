﻿
namespace Aspects.Counters
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    public abstract class CounterAspect : AspectBase
    {
        private static object lockObject = new object();
        
        public string CategoryName { get; set; }

        public string CounterName { get; set; }

        public string CounterInstanceName { get; set; }

        protected string Category
        {
            get
            {
                return this.GetPropertyOrArgumentValue(this.CategoryName) as string;
            }
        }

        protected string Counter
        {
            get
            {
                return this.GetPropertyOrArgumentValue(this.CounterName) as string;
            }
        }

        protected string CounterInstance
        {
            get
            {
                return this.GetPropertyOrArgumentValue(this.CounterInstanceName) as string;
            }
        }

        protected PerformanceCounterType PerformanceCounterType { get; set; } = PerformanceCounterType.NumberOfItems64;

        protected override void OnInitialized()
        {
            CheckParameters();

            lock (lockObject)
            {
                if (PerformanceCounterCategory.Exists(Category)
                && PerformanceCounterCategory.CounterExists(Counter, Category))
                {
                    return;
                }

                if (!PerformanceCounterCategory.Exists(Category))
                {
                    PerformanceCounterCategory.Create(
                          categoryName: Category,
                          categoryHelp: Category,
                          categoryType: PerformanceCounterCategoryType.MultiInstance,
                          counterData: new CounterCreationDataCollection(new CounterCreationData[] {
                                            new CounterCreationData(Counter, Counter, PerformanceCounterType) }));

                    return;
                }

                var category = new PerformanceCounterCategory(Category);

                // Get current counters
                var data = GetCountersData(category);

                var counterDataCollection = GetCreationCollection(category);

                // Add our counter
                counterDataCollection.Add(new CounterCreationData(Counter, Counter, PerformanceCounterType));

                // Remove current category
                PerformanceCounterCategory.Delete(Category);

                // Add all counters
                PerformanceCounterCategory.Create(
                       categoryName: Category,
                       categoryHelp: Category,
                       categoryType: PerformanceCounterCategoryType.MultiInstance,
                       counterData: counterDataCollection);

                // Resets counters data
                this.InitializeCountersData(category, data);
            }
        }

        protected virtual void CheckParameters()
        {
            if (String.IsNullOrEmpty(this.CategoryName))
            {
                throw new ArgumentNullException(nameof(this.CategoryName));
            }

            if (String.IsNullOrEmpty(this.CounterName))
            {
                throw new ArgumentNullException(nameof(this.CounterName));
            }

            if (String.IsNullOrEmpty(this.CounterInstanceName))
            {
                this.CounterInstanceName = Process.GetCurrentProcess().ProcessName;
            }
        }

        private IEnumerable<CounterInstanceValue> GetCountersData(PerformanceCounterCategory category)
        {
            var result = new List<CounterInstanceValue>();

            foreach (var instanceName in category.GetInstanceNames())
            {
                foreach (PerformanceCounter counter in category.GetCounters(instanceName))
                {
                    using (PerformanceCounter counterInstance = new PerformanceCounter(category.CategoryName, counter.CounterName, counter.InstanceName))
                    {
                        result.Add(new CounterInstanceValue
                        {
                            CounterName = counter.CounterName,
                            InstanceName = instanceName,
                            PerformanceCounterType = counter.CounterType,
                            Value = counterInstance.RawValue
                        });
                    }
                }
            }

            return result;
        }

        private CounterCreationDataCollection GetCreationCollection(PerformanceCounterCategory category)
        {
            var result = new CounterCreationDataCollection();

            foreach (var instanceName in category.GetInstanceNames())
            {
                foreach (var counter in category.GetCounters(instanceName))
                {
                    if (HasCounter(result, counter.CounterName))
                    {
                        continue;
                    }

                    result.Add(new CounterCreationData
                    {
                        CounterName = counter.CounterName,
                        CounterHelp = counter.CounterHelp,
                        CounterType = counter.CounterType
                    });
                }
            }

            return result;
        }

        private bool HasCounter(CounterCreationDataCollection source, string counterName)
        {
            foreach (CounterCreationData item in source)
            {
                if (item.CounterName == counterName)
                    return true;
            }

            return false;
        }

        private void InitializeCountersData(PerformanceCounterCategory category, IEnumerable<CounterInstanceValue> data)
        {
            foreach (var item in data)
            {
                using (PerformanceCounter counterInstance = new PerformanceCounter(category.CategoryName, item.CounterName, item.InstanceName, false))
                {
                    counterInstance.RawValue = item.Value;
                }
            }
        }


        class CounterInstanceValue
        {
            public string CounterName { get; set; }

            public PerformanceCounterType PerformanceCounterType { get; set; }

            public string InstanceName { get; set; }

            public long Value { get; set; }
        }
    }
}
