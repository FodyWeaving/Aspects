﻿
namespace Aspects
{
    using Aspects.Crosscuttings.Helpers;
    using MethodDecorator.Fody.Interfaces;
    using System;
    using System.Collections.ObjectModel;
    using System.Reflection;
    using System.Linq;

    public abstract class AspectBase : Attribute, IMethodDecorator
    {
        protected object Instance { get; private set; }

        protected MethodBase Method { get; private set; }

        protected ReadOnlyCollection<string> ArgNames { get; private set; }

        protected ReadOnlyCollection<object> ArgValues { get; private set; }

        protected bool HasReturn
        {
            get
            {
                return ((MethodInfo)this.Method).ReturnType != Type.GetType("System.Void", true);
            }
        }

        public void Init(object instance, MethodBase method, object[] args)
        {
            this.Instance = instance;
            this.Method = method;

            this.ArgNames = new ReadOnlyCollection<string>(method.GetParameters().OrderBy(c => c.Position).Select(c => c.Name).ToArray());
            this.ArgValues = new ReadOnlyCollection<object>(args ?? new object[0]);

            this.OnInitialized();
        }

        protected abstract void OnInitialized();

        public abstract void OnEntry();

        public abstract void OnException(Exception exception);

        public abstract void OnExit();

        /// <summary>
        /// Returns the value for the specified name. 
        /// This will search for the argument values. 
        /// Then will fail back to property instance properties.
        /// The will fall back to juste the name as string value.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        /// <remarks>When using deep names you can use Property.Name.</remarks>
        protected object GetPropertyOrArgumentValue(string name)
        {
            return AspectHelper.GetPropertyOrArgumentValue(Instance, ArgNames, ArgValues, name);
        }
    }
}
