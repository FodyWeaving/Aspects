﻿using Aspects.Crosscuttings.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Text;

namespace Aspects
{
    public static class AspectHelper
    {
        /// <summary>
        /// Returns the value for the specified name. 
        /// This will search for the argument values. 
        /// Then will fail back to property instance properties.
        /// The will fall back to juste the name as string value.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        /// <remarks>When using deep names you can use Property.Name.</remarks>
        public static object GetPropertyOrArgumentValue(object instance, ReadOnlyCollection<string> argNames, ReadOnlyCollection<object> argValues, string name)
        {
            var deepNames = name.Split('.');

            if (deepNames.Length <= 0)
            {
                return null;
            }

            if (argNames.Contains(deepNames[0]))
            {
                return InstancePropertyNameResolver.Resolve(argValues[argNames.IndexOf(deepNames[0])], deepNames, 1);
            }

            if (instance == null)
            {
                return name;
            }

#if PCL
            if(instance.GetType().GetRuntimeProperty(deepNames[0]) != null)
            {
#else
            if (instance.GetType().GetProperty(deepNames[0]) != null)
            {
#endif
                return InstancePropertyNameResolver.Resolve(instance, deepNames);
            }

            return name;
        }
    }
}
