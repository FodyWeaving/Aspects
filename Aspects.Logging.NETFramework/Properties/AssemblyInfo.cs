﻿using log4net.Config;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Aspects.Logging")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Aspects.Logging")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8a60680f-a315-4a19-a527-05fcd3af9030")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: InternalsVisibleTo("LoggingTests, PublicKey=00240000048000009400000006020000002400005253413100040000010001009536177222568a" +
                                                        "bb854d73a9fe8004f2a1c94eea6e730d3dcbfbb806d77498334f0e76041cf25c17b7b8e9273a1a" +
                                                        "6bd8ed1a0a1eb4b7805c9e3629bc7826443663cc4017e1febae068ae4b6b0964592df94f5f7c0d" +
                                                        "ef3684e99c0255d3242ddaaf8786b1730af6a121d9c39c2561571635e5eb9a947b26127afbdffc" +
                                                        "0eaa2498")]


[assembly: XmlConfigurator(Watch = true)]