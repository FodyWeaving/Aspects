﻿
namespace Aspects.Logging
{
    using Aspects.Logging.Context;
    using System;

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Assembly | AttributeTargets.Module)]
    public sealed class ContextAttribute : AspectBase
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public ContextAttribute()
        {
        }

        public ContextAttribute(string key, string value)
        {
            this.Key = key;
            this.Value = value;
        }

        public override void OnEntry()
        {
            ContextManager.SetContextValue(Key, this.GetPropertyOrArgumentValue(this.Value) as string);
        }

        public override void OnException(Exception exception)
        {

        }

        public override void OnExit()
        {

        }

        protected override void OnInitialized()
        {
            ContextManager.PushContextData();
        }
    }
}
