﻿
namespace Aspects.Logging
{
    using Aspects.Logging.Context;
    using System;
    using System.Threading;

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Assembly | AttributeTargets.Module)]
    public sealed class IdentityContextAttribute : AspectBase
    {
        public IdentityContextAttribute()
        {

        }

        public override void OnEntry()
        {
            ContextManager.SetContextValue(Constants.LoggingContextIdentity, Thread.CurrentPrincipal.Identity.Name);
        }

        public override void OnException(Exception exception)
        {

        }

        public override void OnExit()
        {

        }

        protected override void OnInitialized()
        {
            ContextManager.PushContextData();
        }
    }
}
