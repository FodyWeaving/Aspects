﻿
namespace Aspects.Logging.Context
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    internal sealed class ContextData
    {
        public Dictionary<string, string> Data { get; set; }

        public ContextData Parent { get; set; }

        public ContextData(ContextData parent)
        {
            this.Data = new Dictionary<string, string>();
            log4net.LogicalThreadContext.Properties.Clear();

            Parent = parent;

            if (parent == null)
            {
                return;
            }

            foreach (var item in parent.Data)
            {
                this.Data.Add(item.Key, item.Value);
                log4net.LogicalThreadContext.Properties[item.Key] = item.Value;
            }
        }
    }
}
