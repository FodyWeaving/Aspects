﻿namespace Aspects.Logging.Context
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Remoting.Messaging;

    internal static class ContextManager
    {
        internal static string GetContextValue(string key)
        {
            var context = GetCurrentContextData();

            var containsKey = context?.Data.ContainsKey(key);

            if (!containsKey.HasValue || !containsKey.Value)
            {
                return String.Empty;
            }

            return context?.Data[key];
        }

        internal static Dictionary<string, string> GetContextValues()
        {
            var context = GetCurrentContextData();

            return context?.Data;
        }

        internal static void SetContextValue(string key, string value)
        {
            var context = GetCurrentContextData();

            if (context.Data.ContainsKey(key))
            {
                context.Data.Remove(key);
            }

            context.Data.Add(key, value);
        }

        internal static void PopContextData()
        {
            var context = GetCurrentContextData();

            SetCurrentContextData(context.Parent);
        }

        internal static void PushContextData()
        {
            var context = GetCurrentContextData();

            SetCurrentContextData(new ContextData(context));
        }

        private static ContextData GetCurrentContextData()
        {
            var data = CallContext.LogicalGetData(Constants.LoggingContextDataKey);

            if (data == null)
            {
                return null;
            }

            return (data as ContextData) ?? null;
        }

        private static void SetCurrentContextData(ContextData data)
        {
            CallContext.LogicalSetData(Constants.LoggingContextDataKey, data);
        }
    }
}
