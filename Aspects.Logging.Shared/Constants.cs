﻿
namespace Aspects.Logging
{
    public static class Constants
    {
        public const string LoggingContextDataKey = "ASPECTS_LOGGING_CONTEXT_DATA";

        public const string NullText = "null";

        public const string VoidText = "void";

        public const string LoggingContextCorrelationId = "$correlationId";

        public const string LoggingContextExeptionMessage = "$exceptionMessage";

        public const string LoggingContextMethodName = "$methodName";

        public const string LoggingContextMethodCallId = "$methodCallId";

        public const string LoggingContextArgumentValues = "$argumentValues";

        public const string LoggingContextIdentity = "$identity";
    }
}
