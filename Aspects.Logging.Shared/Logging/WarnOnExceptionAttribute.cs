
namespace Aspects.Logging
{
    using Aspects.Logging.Helpers;
    using log4net;
    using MethodDecorator.Fody.Interfaces;
    using System;

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Assembly | AttributeTargets.Module)]
    public sealed class WarnOnExceptionAttribute : AspectBase
    {
        private ILog logger;

        public string Message { get; set; } = $"{Constants.LoggingContextMethodName} failed!";

        protected override void OnInitialized()
        {
            LoggingHelper.LoggingEntry(this.Instance, this.Method, this.ArgNames, this.ArgValues);
            this.logger = LogManager.GetLogger(this.Instance?.GetType());
        }

        public override void OnException(Exception exception)
        {
            this.logger.Warn(LoggingHelper.FormatMessage(this.Message), exception);
        }

        public override void OnEntry()
        {
        }

        public override void OnExit()
        {
        }
    }
}
