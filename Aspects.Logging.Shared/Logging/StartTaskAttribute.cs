﻿
namespace Aspects.Logging
{
    using Aspects.Logging.Context;
    using Aspects.Logging.Helpers;
    using log4net;
    using MethodDecorator.Fody.Interfaces;
    using System;

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Assembly | AttributeTargets.Module)]
    public sealed class StartTaskAttribute : AspectBase
    {
        private ILog logger;

        public string Message { get; set; } = $"Starting task {Constants.LoggingContextCorrelationId}";
        protected override void OnInitialized()
        {
            this.logger = LogManager.GetLogger(this.Instance?.GetType());
        }
        public override void OnEntry()
        {
            LoggingHelper.LoggingEntry(this.Instance, this.Method, this.ArgNames, this.ArgValues);
            ContextManager.SetContextValue(Constants.LoggingContextCorrelationId, Guid.NewGuid().ToString());

            this.logger.Info(LoggingHelper.FormatMessage(this.Message));
        }

        public override void OnException(Exception exception)
        {
        }

        public override void OnExit()
        {
        }
    }
}
