
namespace Aspects.Logging
{
    using Aspects.Logging.Context;
    using Aspects.Logging.Helpers;
    using log4net;
    using MethodDecorator.Fody.Interfaces;
    using System;
    using System.Diagnostics;

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Assembly | AttributeTargets.Module)]
    public sealed class WarnOnTimeExceededAttribute : AspectBase
    {
        private ILog logger;

        private Stopwatch watch;

        public string Timeout { get; set; } = "00.500";

        public string Message { get; set; } = $"Method {Constants.LoggingContextMethodName} has taken more than $timeout, elapsed: $elapsed!";

        protected override void OnInitialized()
        {
            LoggingHelper.LoggingEntry(this.Instance, this.Method, this.ArgNames, this.ArgValues);
            this.logger = LogManager.GetLogger(this.Instance?.GetType());
        }

        public override void OnEntry()
        {
            watch = Stopwatch.StartNew();
        }

        public override void OnExit()
        {
            watch.Stop();

            TimeSpan timeout;

            if (!TimeSpan.TryParse(this.Timeout, out timeout))
            {
                this.logger.Warn($"Cannot parse {Timeout} to TimeSpan for WarnOnTimeExceeded aspect!");
                return;
            }
            
            if (watch.Elapsed < timeout)
                return;

            ContextManager.SetContextValue("$timeout", timeout.ToString());
            ContextManager.SetContextValue("$elapsed", watch.Elapsed.ToString());

            this.logger.Warn(LoggingHelper.FormatMessage(this.Message));
        }

        public override void OnException(Exception exception)
        {
        }
    }
}
