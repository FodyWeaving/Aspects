
namespace Aspects.Logging
{
    using Aspects.Logging.Helpers;
    using log4net;
    using System;

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Assembly | AttributeTargets.Module)]
    public sealed class InfoAttribute : AspectBase
    {
        private ILog logger;

        public string Message { get; set; } = String.Empty;

        public InfoAttribute()
        {
        }

        protected override void OnInitialized()
        {
            LoggingHelper.LoggingEntry(this.Instance, this.Method, this.ArgNames, this.ArgValues);
            this.logger = LogManager.GetLogger(this.Instance?.GetType());
        }

        public override void OnEntry()
        {
            
        }

        public override void OnException(Exception exception)
        {
            
        }

        public override void OnExit()
        {
            this.logger.Info(LoggingHelper.FormatMessage(this.Message));
        }
    }
}
