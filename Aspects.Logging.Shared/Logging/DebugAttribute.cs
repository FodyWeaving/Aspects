
namespace Aspects.Logging
{
    using Aspects.Logging.Context;
    using Aspects.Logging.Helpers;
    using log4net;
    using MethodDecorator.Fody.Interfaces;
    using System;

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Assembly | AttributeTargets.Module)]
    public sealed class DebugAttribute : AspectBase
    {
        private ILog logger;

        public string EntryMessage { get; set; } = $"{Constants.LoggingContextMethodName} called with id {Constants.LoggingContextMethodCallId} and arguments: $argumentValues.";

        public string ErrorMessage { get; set; } = $"{Constants.LoggingContextMethodName} with id {Constants.LoggingContextMethodCallId} failed with exception \"{Constants.LoggingContextExeptionMessage}\".";

        public string ExitMessage { get; set; } = $"{Constants.LoggingContextMethodName} with id {Constants.LoggingContextMethodCallId} exited.";

        protected override void OnInitialized()
        {
            this.logger = LogManager.GetLogger(this.Instance?.GetType());
        }

        public override void OnEntry()
        {
            LoggingHelper.LoggingEntry(this.Instance, this.Method, this.ArgNames, this.ArgValues);
            this.logger.Debug(LoggingHelper.FormatMessage(EntryMessage));
        }

        public override void OnException(Exception exception)
        {
            ContextManager.SetContextValue(Constants.LoggingContextExeptionMessage, exception?.Message ?? Constants.NullText);

            this.logger.Debug(LoggingHelper.FormatMessage(ErrorMessage));

            ContextManager.PopContextData();
        }

        public override void OnExit()
        {
            this.logger.Debug(LoggingHelper.FormatMessage(ExitMessage));
        }
    }
}
