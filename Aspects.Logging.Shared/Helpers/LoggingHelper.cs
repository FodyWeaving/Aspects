﻿using Aspects.Logging.Context;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Aspects.Logging.Helpers
{
    internal static class LoggingHelper
    {
        internal static string FormatMessage(string message)
        {
            PopulateLog4NetContext();

            var result = new StringBuilder(message);

            var searchPattern = new Regex(@"\s*(?<Replacement>[$]\w*)\s*?", RegexOptions.Compiled | RegexOptions.Multiline);
            var tokenPattern = new Regex(@"\s*(?<Token>[$]\w*)\s*?", RegexOptions.Compiled);

            var context = ContextManager.GetContextValues();

            if (context == null)
            {
                return result.ToString();
            }

            var match = searchPattern.Matches(message);

            foreach (Match item in match)
            {
                var group = item.Groups["Replacement"];

                var key = tokenPattern.Matches(group.Value)[0].Groups["Token"].Value;

                if (!context.ContainsKey(key))
                {
                    continue;
                }

                result = result
                     .Replace(group.Value, context[key]);
            }

            return result.ToString();
        }

        internal static string FormatItemNameAndValue(string name, object value)
        {
            return $"{(String.IsNullOrEmpty(name) ? String.Empty : name + " ")}'{value?.ToString() ?? Constants.NullText}' ({value?.GetType()})";
        }

        internal static void LoggingEntry(object instance, MethodBase method, ReadOnlyCollection<string> argNames, ReadOnlyCollection<object> argValues)
        {
            ContextManager.PushContextData();

            ContextManager.SetContextValue(Constants.LoggingContextMethodName, method?.Name);
            ContextManager.SetContextValue(Constants.LoggingContextMethodCallId, Guid.NewGuid().ToString());

            var builder = new StringBuilder();

            var argNameAndValues = new List<String>();

            foreach (var item in argNames)
            {
                var argumentValue = AspectHelper.GetPropertyOrArgumentValue(instance, argNames, argValues, item);
                ContextManager.SetContextValue($"${item}", argumentValue?.ToString() ?? Constants.NullText);

                argNameAndValues.Add(FormatItemNameAndValue(item, argumentValue));
            }

            ContextManager.SetContextValue(Constants.LoggingContextArgumentValues, String.Join(", ", argNameAndValues.ToArray()));
        }

        private static void PopulateLog4NetContext()
        {
            if (ContextManager.GetContextValues() == null)
            {
                return;
            }

            foreach (var item in ContextManager.GetContextValues())
            {
                LogicalThreadContext.Properties[item.Key] = item.Value;
            }
        }
    }
}
