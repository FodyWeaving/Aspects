﻿
namespace AssemblyToProcess
{
    using AssemblyToProcess.Aspects;
    using System;

    public class SimpleClass
    {
        public int TestValue
        {
            get; set;
        }

        [SimpleAspect]
        public void TestWithNoResult()
        {
            var test = "";

            return;
        }

        [SimpleAspect]
        public int TestWithResult()
        {
            return 0;
        }

        [SimpleAspect]
        public int TestWithMultipleResults()
        {
            if (TestValue == 1)
            {
                return 0;
            }
            else if (TestValue > 2)
            {
                return 1;
            }

            return 3;
        }

        [SimpleAspect]
        public int TestWithException()
        {
            throw new NotImplementedException();
        }
    }
}
