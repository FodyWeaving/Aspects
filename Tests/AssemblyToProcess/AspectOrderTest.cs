﻿using AssemblyToProcess.Aspects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyToProcess
{
    public class AspectOrderTest
    {
        [SimpleAspect]
        [AspectWithProperties]
        public void SimpleMethod()
        {

        }
    }
}
