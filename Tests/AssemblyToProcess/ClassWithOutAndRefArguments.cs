﻿using AssemblyToProcess.Aspects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyToProcess
{
    public class ClassWithOutAndRefArguments
    {
        [SimpleAspect]
        public void TestWithOutArgument(int arg1, string arg2, out object arg3)
        {
            arg3 = new { };

            return;
        }

        [SimpleAspect]
        public void TestWithRefArgument(int arg1, ref int arg2, int arg3 )
        {
            return;
        }
    }
}
