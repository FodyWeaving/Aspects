﻿
namespace AssemblyToProcess
{
    using System;

    public class InjectedClass
    {
        public int TestValue
        {
            get; set;
        }

        public void TestWithNoResult()
        {
            var test = "";

            return;
        }
        
        public int TestWithResult()
        {
            return 0;
        }
    }
}
