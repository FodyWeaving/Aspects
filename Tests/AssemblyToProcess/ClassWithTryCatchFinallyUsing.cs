﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssemblyToProcess.Aspects;
using System.IO;
using System.Reflection;

namespace AssemblyToProcess
{
    public class ClassWithTryCatchFinallyUsing
    {
        [SimpleAspect]
        public void TryFinallyTest()
        {
            try
            {

            }
            finally
            {

            }
        }

        [SimpleAspect]
        public void UsingTest()
        {
            using (var memoryStream = new MemoryStream())
            {

            }
        }

        [SimpleAspect]
        public void TryCatchTest()
        {
            try
            {

            }
            catch (Exception e)
            {

            }
        }

        [SimpleAspect]
        public void TryCatchFinallyTest()
        {
            try
            {

            }
            catch (Exception e)
            {

            }
            finally
            {

            }
        }

        [SimpleAspect]
        public void TryCatchFinallyNestedUsingTest()
        {
            try
            {
                using (var memoryStream = new MemoryStream())
                {

                }
            }
            catch (Exception e)
            {

            }
            finally
            {

            }
        }

        [SimpleAspect]
        public void NestedTryCatch()
        {
            try
            {
                try
                {
                    using (var memoryStream = new MemoryStream())
                    {

                    }
                }
                catch (Exception e)
                {

                }
                finally
                {

                }
            }
            catch (Exception e)
            {

            }
            finally
            {

            }
        }

        [SimpleAspect]
        public void NestedTryCatchWithMultipleReturns()
        {
            try
            {
                if (DateTime.Now.Day == 1)
                {
                    return;
                }
                else 

                try
                {
                    if (DateTime.Now.Day > 2)
                    {
                        return;
                    }

                    using (var memoryStream = new MemoryStream())
                    {
                        return;
                    }
                }
                catch (Exception e)
                {

                }
                finally
                {

                }
            }
            catch (Exception e)
            {

            }
            finally
            {

            }
        }
    }
}
