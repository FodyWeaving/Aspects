﻿using System;
using NUnit.Framework;
using AssemblyToProcess;
using AssemblyToProcess.Aspects;
using System.Linq;

namespace WeavedAssemblyTest
{
    [TestFixture]
    public class SimpleTests
    {
        [Test]
        public void TestWithNoResultWeaved()
        {
            var instance = new SimpleClass();

            instance.TestWithNoResult();

            var aspectData = AspectDataAggregator.Current[instance] as AspectInstanceData;

            Assert.IsNotNull(aspectData);

            Assert.AreEqual(1, aspectData.Methods.Count);

            var methodAspectedData = aspectData[typeof(SimpleClass).GetMethod("TestWithNoResult")];
            Assert.IsNotNull(methodAspectedData);

            Assert.IsEmpty(methodAspectedData.ArgNames);
            Assert.IsEmpty(methodAspectedData.ArgValues);
            Assert.AreEqual(0, methodAspectedData.Exceptions.Count());
            Assert.AreEqual(1, methodAspectedData.EntryCount);
            Assert.AreEqual(1, methodAspectedData.Results.Count());
            Assert.AreEqual(null, methodAspectedData.Results.Single());
        }

        [Test]
        public void TestWithResultWeaved()
        {
            var instance = new SimpleClass();

            var methodResult = instance.TestWithResult();

            var aspectData = AspectDataAggregator.Current[instance] as AspectInstanceData;

            Assert.IsNotNull(aspectData);

            Assert.AreEqual(1, aspectData.Methods.Count);

            var methodAspectedData = aspectData[typeof(SimpleClass).GetMethod("TestWithResult")];
            Assert.IsNotNull(methodAspectedData);

            Assert.IsEmpty(methodAspectedData.ArgNames);
            Assert.IsEmpty(methodAspectedData.ArgValues);
            Assert.AreEqual(0, methodAspectedData.Exceptions.Count());
            Assert.AreEqual(1, methodAspectedData.EntryCount);
            Assert.AreEqual(1, methodAspectedData.Results.Count());
            Assert.AreEqual(null, methodAspectedData.Results.Single());
        }

        [Test]
        public void TestWithMultipleResults()
        {
            Action<int, int> testWithMultipleOutputAction = (testValue, expectedOutput) =>
            {
                var instance = new SimpleClass();

                instance.TestValue = testValue;

                var methodResult = instance.TestWithResult();

                var aspectData = AspectDataAggregator.Current[instance] as AspectInstanceData;

                Assert.IsNotNull(aspectData);

                Assert.AreEqual(1, aspectData.Methods.Count);

                var methodAspectedData = aspectData[typeof(SimpleClass).GetMethod("TestWithResult")];
                Assert.IsNotNull(methodAspectedData);

                Assert.IsEmpty(methodAspectedData.ArgNames);
                Assert.IsEmpty(methodAspectedData.ArgValues);
                Assert.AreEqual(0, methodAspectedData.Exceptions.Count());
                Assert.AreEqual(1, methodAspectedData.EntryCount);
                Assert.AreEqual(1, methodAspectedData.Results.Count());
                Assert.AreEqual(null, methodAspectedData.Results.Single());
            };

            testWithMultipleOutputAction(1, 0);
            testWithMultipleOutputAction(2, 3);
            testWithMultipleOutputAction(3, 1);
        }

        [Test]
        public void TestWithExceptionWeaved()
        {
            var instance = new SimpleClass();
            bool hasException = false;

            try
            {
                var methodResult = instance.TestWithException();
            }
            catch (Exception e)
            {
                hasException = true;
                var aspectData = AspectDataAggregator.Current[instance] as AspectInstanceData;

                Assert.IsNotNull(aspectData);

                Assert.AreEqual(1, aspectData.Methods.Count);

                var methodAspectedData = aspectData[typeof(SimpleClass).GetMethod("TestWithException")];
                Assert.IsNotNull(methodAspectedData);

                Assert.IsEmpty(methodAspectedData.ArgNames);
                Assert.IsEmpty(methodAspectedData.ArgValues);
                Assert.AreEqual(1, methodAspectedData.Exceptions.Count());
                Assert.AreEqual(1, methodAspectedData.EntryCount);
                Assert.AreEqual(0, methodAspectedData.Results.Count());
                Assert.IsTrue(methodAspectedData.Exceptions.Single() is NotImplementedException);
                Assert.AreEqual(e, methodAspectedData.Exceptions.Single());
            }
            finally
            {
                Assert.IsTrue(hasException, "The methods didn't throw the exception!");
            }
        }
    }
}
