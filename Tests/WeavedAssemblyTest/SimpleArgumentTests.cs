﻿using NUnit.Framework;
using AssemblyToProcess;
using AssemblyToProcess.Aspects;
using System.Linq;

namespace WeavedAssemblyTest
{
    [TestFixture]
    public class SimpleArgumentTests
    {
        [Test]
        public void TestArgumentsWithNoResultWeaved()
        {
            var instance = new ClassWithArguments();
            var objArg = new { };

            instance.TestWithNoResult(5, "Test value", objArg);

            var aspectData = AspectDataAggregator.Current[instance] as AspectInstanceData;

            Assert.IsNotNull(aspectData);

            Assert.AreEqual(1, aspectData.Methods.Count);

            var methodAspectedData = aspectData[typeof(ClassWithArguments).GetMethod("TestWithNoResult")];
            Assert.IsNotNull(methodAspectedData);

            Assert.IsNotNull(methodAspectedData.ArgNames);
            Assert.IsNotNull(methodAspectedData.ArgValues);

            var argNames = methodAspectedData.ArgNames.ToArray();
            var argValues = methodAspectedData.ArgValues.ToArray();

            Assert.AreEqual("arg1", argNames[0]);
            Assert.AreEqual(5, argValues[0]);

            Assert.AreEqual("arg2", argNames[1]);
            Assert.AreEqual("Test value", argValues[1]);

            Assert.AreEqual("arg3", argNames[2]);
            Assert.AreEqual(objArg, argValues[2]);

            Assert.AreEqual(0, methodAspectedData.Exceptions.Count());
            Assert.AreEqual(1, methodAspectedData.EntryCount);
            Assert.AreEqual(1, methodAspectedData.Results.Count());
            Assert.AreEqual(null, methodAspectedData.Results.Single());
        }
    }
}
