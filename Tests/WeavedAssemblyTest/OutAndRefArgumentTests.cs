﻿using NUnit.Framework;
using AssemblyToProcess;
using AssemblyToProcess.Aspects;
using System.Linq;

namespace WeavedAssemblyTest
{
    [TestFixture]
    public class OutAndRefArgumentTests
    {
        [Test]
        public void TestWithOutArgumentWeaved()
        {
            var instance = new ClassWithOutAndRefArguments();

            object outResult;

            instance.TestWithOutArgument(5, "Test value", out outResult);

            var aspectData = AspectDataAggregator.Current[instance] as AspectInstanceData;

            Assert.IsNotNull(aspectData);

            Assert.AreEqual(1, aspectData.Methods.Count);

            var methodAspectedData = aspectData[typeof(ClassWithOutAndRefArguments).GetMethod("TestWithOutArgument")];
            Assert.IsNotNull(methodAspectedData);

            Assert.IsNotNull(methodAspectedData.ArgNames);
            Assert.IsNotNull(methodAspectedData.ArgValues);

            var argNames = methodAspectedData.ArgNames.ToArray();
            var argValues = methodAspectedData.ArgValues.ToArray();

            Assert.AreEqual("arg1", argNames[0]);
            Assert.AreEqual(5, argValues[0]);

            Assert.AreEqual("arg2", argNames[1]);
            Assert.AreEqual("Test value", argValues[1]);

            Assert.AreEqual(0, methodAspectedData.Exceptions.Count());
            Assert.AreEqual(1, methodAspectedData.EntryCount);
            Assert.AreEqual(1, methodAspectedData.Results.Count());
            Assert.AreEqual(null, methodAspectedData.Results.Single());
        }

        [Test]
        public void TestWithRefArgumentWeaved()
        {
            var instance = new ClassWithOutAndRefArguments();
            int refInt = 6;

            instance.TestWithRefArgument(5, ref refInt, 4);

            var aspectData = AspectDataAggregator.Current[instance] as AspectInstanceData;

            Assert.IsNotNull(aspectData);

            Assert.AreEqual(1, aspectData.Methods.Count);

            var methodAspectedData = aspectData[typeof(ClassWithOutAndRefArguments).GetMethod("TestWithRefArgument")];
            Assert.IsNotNull(methodAspectedData);

            Assert.IsNotNull(methodAspectedData.ArgNames);
            Assert.IsNotNull(methodAspectedData.ArgValues);

            var argNames = methodAspectedData.ArgNames.ToArray();
            var argValues = methodAspectedData.ArgValues.ToArray();

            Assert.AreEqual("arg1", argNames[0]);
            Assert.AreEqual(5, argValues[0]);

            Assert.AreEqual("arg2", argNames[1]);
            Assert.AreEqual(refInt, argValues[1]);

            Assert.AreEqual("arg3", argNames[2]);
            Assert.AreEqual(4, argValues[2]);

            Assert.AreEqual(0, methodAspectedData.Exceptions.Count());
            Assert.AreEqual(1, methodAspectedData.EntryCount);
            Assert.AreEqual(1, methodAspectedData.Results.Count());
            Assert.AreEqual(null, methodAspectedData.Results.Single());
        }
    }
}
