﻿using System;
using NUnit.Framework;
using AssemblyToProcess;
using AssemblyToProcess.Aspects;
using System.Linq;

namespace WeavedAssemblyTest
{
    [TestFixture]
    public class TryCatchFinallyAndUsingTests
    {
        [Test]
        public void TryFinallyTest()
        {
            var instance = new ClassWithTryCatchFinallyUsing();

            instance.TryFinallyTest();

            var aspectData = AspectDataAggregator.Current[instance] as AspectInstanceData;

            Assert.IsNotNull(aspectData);

            Assert.AreEqual(1, aspectData.Methods.Count);

            var methodAspectedData = aspectData[typeof(ClassWithTryCatchFinallyUsing).GetMethod("TryFinallyTest")];
            Assert.IsNotNull(methodAspectedData);

            Assert.IsEmpty(methodAspectedData.ArgNames);
            Assert.IsEmpty(methodAspectedData.ArgValues);
            Assert.AreEqual(0, methodAspectedData.Exceptions.Count());
            Assert.AreEqual(1, methodAspectedData.EntryCount);
            Assert.AreEqual(1, methodAspectedData.Results.Count());
        }

        [Test]
        public void UsingTest()
        {
            var instance = new ClassWithTryCatchFinallyUsing();

            instance.UsingTest();

            var aspectData = AspectDataAggregator.Current[instance] as AspectInstanceData;

            Assert.IsNotNull(aspectData);

            Assert.AreEqual(1, aspectData.Methods.Count);

            var methodAspectedData = aspectData[typeof(ClassWithTryCatchFinallyUsing).GetMethod("UsingTest")];
            Assert.IsNotNull(methodAspectedData);

            Assert.IsEmpty(methodAspectedData.ArgNames);
            Assert.IsEmpty(methodAspectedData.ArgValues);
            Assert.AreEqual(0, methodAspectedData.Exceptions.Count());
            Assert.AreEqual(1, methodAspectedData.EntryCount);
            Assert.AreEqual(1, methodAspectedData.Results.Count());
        }

        [Test]
        public void TryCatchTest()
        {
            var instance = new ClassWithTryCatchFinallyUsing();

            instance.TryCatchTest();

            var aspectData = AspectDataAggregator.Current[instance] as AspectInstanceData;

            Assert.IsNotNull(aspectData);

            Assert.AreEqual(1, aspectData.Methods.Count);

            var methodAspectedData = aspectData[typeof(ClassWithTryCatchFinallyUsing).GetMethod("TryCatchTest")];
            Assert.IsNotNull(methodAspectedData);

            Assert.IsEmpty(methodAspectedData.ArgNames);
            Assert.IsEmpty(methodAspectedData.ArgValues);
            Assert.AreEqual(0, methodAspectedData.Exceptions.Count());
            Assert.AreEqual(1, methodAspectedData.EntryCount);
            Assert.AreEqual(1, methodAspectedData.Results.Count());
            Assert.AreEqual(null, methodAspectedData.Results.Single());
        }

        [Test]
        public void TryCatchFinallyTest()
        {
            var instance = new ClassWithTryCatchFinallyUsing();

            instance.TryCatchFinallyTest();

            var aspectData = AspectDataAggregator.Current[instance] as AspectInstanceData;

            Assert.IsNotNull(aspectData);

            Assert.AreEqual(1, aspectData.Methods.Count);

            var methodAspectedData = aspectData[typeof(ClassWithTryCatchFinallyUsing).GetMethod("TryCatchFinallyTest")];
            Assert.IsNotNull(methodAspectedData);

            Assert.IsEmpty(methodAspectedData.ArgNames);
            Assert.IsEmpty(methodAspectedData.ArgValues);
            Assert.AreEqual(0, methodAspectedData.Exceptions.Count());
            Assert.AreEqual(1, methodAspectedData.EntryCount);
            Assert.AreEqual(1, methodAspectedData.Results.Count());
        }

        [Test]
        public void TryCatchFinallyNestedUsingTest()
        {
            var instance = new ClassWithTryCatchFinallyUsing();

            instance.TryCatchFinallyNestedUsingTest();

            var aspectData = AspectDataAggregator.Current[instance] as AspectInstanceData;

            Assert.IsNotNull(aspectData);

            Assert.AreEqual(1, aspectData.Methods.Count);

            var methodAspectedData = aspectData[typeof(ClassWithTryCatchFinallyUsing).GetMethod("TryCatchFinallyNestedUsingTest")];
            Assert.IsNotNull(methodAspectedData);

            Assert.IsEmpty(methodAspectedData.ArgNames);
            Assert.IsEmpty(methodAspectedData.ArgValues);
            Assert.AreEqual(0, methodAspectedData.Exceptions.Count());
            Assert.AreEqual(1, methodAspectedData.EntryCount);
            Assert.AreEqual(1, methodAspectedData.Results.Count());
        }

        [Test]
        public void NestedTryCatch()
        {
            var instance = new ClassWithTryCatchFinallyUsing();

            instance.NestedTryCatch();

            var aspectData = AspectDataAggregator.Current[instance] as AspectInstanceData;

            Assert.IsNotNull(aspectData);

            Assert.AreEqual(1, aspectData.Methods.Count);

            var methodAspectedData = aspectData[typeof(ClassWithTryCatchFinallyUsing).GetMethod("NestedTryCatch")];
            Assert.IsNotNull(methodAspectedData);

            Assert.IsEmpty(methodAspectedData.ArgNames);
            Assert.IsEmpty(methodAspectedData.ArgValues);
            Assert.AreEqual(0, methodAspectedData.Exceptions.Count());
            Assert.AreEqual(1, methodAspectedData.EntryCount);
            Assert.AreEqual(1, methodAspectedData.Results.Count());
        }
    }
}
