﻿
namespace AspectWeavingTest
{
    using Aspects;
    using AssemblyToProcess;
    using AssemblyToProcess.Aspects;
    using NUnit.Framework;
    using System.Linq;

    [TestFixture]
    public class AssemblyReferenceTests
    {
        [Test]
        public void VerifyThatAspectsDoesntImplementIAspect()
        {
            Assert.IsFalse(typeof(SimpleAspectAttribute).GetInterfaces()
                   .Any(x => x == typeof(IAspect)), $"SimpleAspectAttribute implements IAspect!");

            Assert.IsFalse(typeof(AspectWithPropertiesAttribute).GetInterfaces()
                   .Any(x => x == typeof(IAspect)), $"AspectWithPropertiesAttribute implements IAspect!");
        }

        [Test]
        public void VerifyThatAssembliesDoesntReferenceAspectAssembly()
        {
            var aspectAssembly = typeof(IAspect).Assembly;

            var query = typeof(SimpleAspectAttribute)
                .Assembly
                .GetReferencedAssemblies()
                .Where(x => x.FullName == aspectAssembly.FullName);

            Assert.IsFalse(query.Any(), $"{typeof(SimpleAspectAttribute).Assembly} references Aspects assembly!");

            query = typeof(ClassWithArguments)
               .Assembly
               .GetReferencedAssemblies()
               .Where(x => x.FullName == aspectAssembly.FullName);

            Assert.IsFalse(query.Any(), $"{typeof(ClassWithArguments).Assembly} references Aspects assembly!");
        }
    }
}
