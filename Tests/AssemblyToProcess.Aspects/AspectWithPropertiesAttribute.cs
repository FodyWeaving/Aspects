﻿
namespace AssemblyToProcess.Aspects
{
    using MethodDecorator.Fody.Interfaces;
    using System;
    using System.Linq;
    using System.Reflection;

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Assembly | AttributeTargets.Module)]
    public class AspectWithPropertiesAttribute : Attribute, IMethodDecorator
    {
        public string StringParameter { get; set; }

        public int NumericParameter { get; set; }

        public int Order => 0;

        private object instance;

        private MethodBase method;

        public void Init(object instance, MethodBase method, object[] args)
        {
            this.instance = instance;
            this.method = method;

            var data = new AspectInstanceData(this, instance);

            var argNames = method.GetParameters().OrderBy(c => c.Position).Select(c => c.Name).ToArray();

            data.Methods.Add(new AspectInstanceMethodData
            {
                Method = this.method,
                ArgNames = argNames,
                ArgValues = args
            });

            AspectDataAggregator.Current.AspectInstances.Add(data);
        }

        public void OnEntry()
        {
            AspectDataAggregator.Current[this.instance][this.method].EntryCount++;
        }

        public void OnException(Exception exception)
        {
            AspectDataAggregator.Current[this.instance][this.method].Exceptions.Add(exception);
        }

        public void OnExit()
        {
            AspectDataAggregator.Current[this.instance][this.method].Results.Add(null);
        }
    }

}
