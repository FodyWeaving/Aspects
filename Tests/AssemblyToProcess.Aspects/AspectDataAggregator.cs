﻿
namespace AssemblyToProcess.Aspects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public class AspectInstanceData
    {
        public AspectInstanceData(object attributeInstance, object instance)
        {
            this.AspectInstance = attributeInstance;
            this.Instance = instance;
            this.Methods = new List<AspectInstanceMethodData>();
        }

        public object Instance { get; }

        public object AspectInstance { get; }

        public IList<AspectInstanceMethodData> Methods { get; set; }

        public AspectInstanceMethodData this[MethodBase method]
        {
            get
            {
                return this.Methods.Single(x => x.Method == method);
            }
        }
    }

    public class AspectInstanceMethodData
    {
        public AspectInstanceMethodData()
        {
            this.Exceptions = new List<Exception>();
            this.Results = new List<object>();
        }

        public MethodBase Method { get; set; }

        public IEnumerable<string> ArgNames { get; set; }

        public IEnumerable<object> ArgValues { get; set; }

        public int EntryCount { get; set; }

        public IList<Exception> Exceptions { get; set; }

        public IList<object> Results { get; set; }
    }

    public class AspectDataAggregator
    {
        private static AspectDataAggregator _current;

        public AspectInstanceData this[object instance]
        {
            get
            {
                return AspectInstances.Single(x => x.Instance == instance);
            }
        }

        public static AspectDataAggregator Current
        {
            get
            {
                _current = _current ?? new AspectDataAggregator();
                return _current;
            }
        }

        public IList<AspectInstanceData> AspectInstances { get; }

        private AspectDataAggregator()
        {
            this.AspectInstances = new List<AspectInstanceData>();
        }

        static void Reset()
        {
            _current = new AspectDataAggregator();
        }
    }
}
