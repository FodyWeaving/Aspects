﻿
namespace LoggingTests
{
    using Aspects.Logging;
    using Aspects.Logging.Context;
    using NUnit.Framework;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    [TestFixture]
    public class ContextTests
    {
        public string PropertyValue { get; set; } = "MyProperty value";

        public class Foo
        {
            public string Bar { get; set; } = "Foo bar";
        }

        public Foo Bar { get; set; } = new Foo();

        public class AsyncState
        {
            public Exception Exception { get; set; }

            public bool Finished { get; set; } = false;
        }

        [Test]
        [Context("Key1", "MyValue")]
        public void Context_Method_ShouldContainsConstant()
        {
            Assert.AreEqual("MyValue", ContextManager.GetContextValue("Key1"));
        }

        [Test]
        public void Context_Method_ShouldContainsArgumentValue()
        {
            TestMethodWithArgument("argument value");
        }

        [Test]
        [Context("Key1", "PropertyValue")]
        public void Context_Method_ShouldContainsPropertyValue()
        {
            Assert.AreEqual(PropertyValue, ContextManager.GetContextValue("Key1"));
        }

        [Test]
        [Context("Key1", "Bar.Bar")]
        public void Context_Method_ShouldContainsDeepPropertyValue()
        {
            Assert.AreEqual(Bar.Bar, ContextManager.GetContextValue("Key1"));
        }

        [Test]
        public void Context_Method_ShouldContainsDeepArgumentValue()
        {
            TestMethodWithArgument(Bar);
        }

        [Test]
        [Context("Key1", "MyValue")]
        public void Context_Method_ShouldContainsParentValues()
        {
            TestMethodForInheritance();
        }

        [Test]
        [Context("Key1", "MyValue")]
        public async Task Context_Task_ShouldContainsParentValues()
        {
            await Task.Factory.StartNew(TestMethodForInheritance);
        }

        [Test]
        [Context("Key1", "MyValue")]
        public void Context_ThreadPool_ShouldContainsParentValues()
        {
            var state = new AsyncState();

            ThreadPool.QueueUserWorkItem(a =>
            {
                try
                {
                    TestMethodForInheritance();

                }
                catch (Exception e)
                {
                    (a as AsyncState).Exception = e;
                }
                finally
                {
                    (a as AsyncState).Finished = true;
                }
            }, state);

            while (!state.Finished)
                continue;

            Assert.IsTrue(state.Finished);
            Assert.IsNull(state.Exception);
        }

        [Context("Key2", "valueTest")]
        private void TestMethodForInheritance()
        {
            Assert.AreEqual("MyValue", ContextManager.GetContextValue("Key1"));
            Assert.AreEqual("valueTest", ContextManager.GetContextValue("Key2"));
        }

        [Context("Key1", "argumentTest")]
        private void TestMethodWithArgument(string argumentTest)
        {
            Assert.AreEqual(argumentTest, ContextManager.GetContextValue("Key1"));
        }

        [Context("Key1", "foo.Bar")]
        private void TestMethodWithArgument(Foo foo)
        {
            Assert.AreEqual(foo.Bar, ContextManager.GetContextValue("Key1"));
        }
    }
}
