﻿

namespace LoggingTests
{
    using Aspects.Logging;
    using NUnit.Framework;
    using System;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Threading;

    [TestFixture]
    public class LoggingTests
    {
        private string LogFilePath { get; set; } = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "LoggingTests.log");

        [SetUp]
        public void Initialize()
        {
            if (File.Exists(LogFilePath))
                File.Delete(LogFilePath);
        }

        #region ----- Api logs -----

        [Test]
        [StartTask]
        public void Logging_ApiLogShouldWriteStartApi()
        {
            AssertMatch(@"^Starting task [a-zA-Z0-9-]*$");
        }

        #endregion

        #region ----- Verbose logs -----

        [Test]
        [Debug]
        public void Logging_VerboseLogShouldWriteCall()
        {
            AssertMatch(@"^Logging_VerboseLogShouldWriteCall called with id [a-z0-9-]* and arguments: [.]$", 0);
        }

        [Test]
        public void Logging_VerboseLogShouldLogException()
        {
            try
            {
                Logging_VerboseLogWithException();
            }
            catch (NotImplementedException)
            {

            }
            finally
            {
                AssertMatch(@"^Logging_VerboseLogWithException called with id [a-z0-9-]* and arguments: [.]$", 0);
                AssertMatch(@"^Logging_VerboseLogWithException with id [a-z0-9-]* failed with exception ""The method or operation is not implemented."".$", 1);
            }
        }

        [Test]
        public void Logging_VerboseLogShouldLogReturnVoidStatement()
        {
            Logging_VerboseLogWithNoResult();

            AssertMatch(@"^Logging_VerboseLogWithNoResult called with id [a-z0-9-]* and arguments: [.]$", 0);
            AssertMatch(@"^Logging_VerboseLogWithNoResult with id [a-z0-9-]* exited[.]$", 1);
        }

        [Test]
        public void Logging_VerboseLogShouldLogReturnValueStatement()
        {
            Logging_VerboseLogWithReturnValue();

            AssertMatch(@"^Logging_VerboseLogWithReturnValue called with id [a-z0-9-]* and arguments: [.]$", 0);
            AssertMatch(@"^Logging_VerboseLogWithReturnValue with id [a-z0-9-]* exited[.]$", 1);
        }

        [Test]
        public void Logging_VerboseLogShouldLogArgumentsStatement()
        {
            Logging_VerboseLogWithArguments("value1", "value2");

            AssertMatch(@"^Logging_VerboseLogWithArguments called with id [a-z0-9-]* and arguments: arg1 'value1' [(][a-zA-Z0-9.]*[)], arg2 'value2' [(][a-zA-Z0-9.]*[)][.]$", 0);
            AssertMatch(@"^Logging_VerboseLogWithArguments with id [a-z0-9-]* exited[.]$", 1);
        }

        [Debug]
        private void Logging_VerboseLogWithNoResult()
        {
            int i = 0;
            int y = i + 2;

            DateTime.Now.AddDays(y);
        }

        [Debug]
        private void Logging_VerboseLogWithException()
        {
            throw new NotImplementedException();
        }

        [Debug]
        private int Logging_VerboseLogWithReturnValue()
        {
            return 0;
        }

        [Debug]
        private void Logging_VerboseLogWithArguments(string arg1, string arg2)
        {
        }

        #endregion

        #region ----- Event logs -----

        [Test]
        public void Logging_EventLogShouldLogEventStatement()
        {
            Logging_EventLogMethod();

            AssertMatch(@"^MyEvent$", 0);
        }

        [Info(Message = "MyEvent")]
        public void Logging_EventLogMethod()
        {

        }

        #endregion

        #region ----- Warn on exception logs -----

        [Test]
        public void Logging_WarnOnExceptionShouldLogExceptionStatement()
        {
            try
            {
                Logging_WarnOnExceptionThrowMethod();
            }
            catch (NotImplementedException)
            {

            }
            finally
            {
                AssertMatch(@"^Logging_WarnOnExceptionThrowMethod failed!", 0);

            }
        }

        [WarnOnException]
        private void Logging_WarnOnExceptionThrowMethod()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ----- Warn on time exceeded logs -----

        [Test]
        public void Logging_WarnOnTimeExceededShouldLogStatement()
        {
            Logging_WarnOnTimeExceededMethod();

            AssertMatch(@"^Method Logging_WarnOnTimeExceededMethod has taken more than (\d{2}:\d{2}:\d{2}.\d{7}), elapsed: (\d{2}:\d{2}:\d{2}.\d{7})!", 0);
        }

        [WarnOnTimeExceeded(Timeout = "00:00:00.100")]
        private void Logging_WarnOnTimeExceededMethod()
        {
            Thread.Sleep(TimeSpan.FromSeconds(1));
        }

        #endregion

        private void AssertMatch(string regex, int logIndex = 0)
        {
            var contentRegex = new Regex(regex);

            FileAssert.Exists(LogFilePath);

            var fileContent = File.ReadAllLines(LogFilePath);

            Assert.IsTrue(contentRegex.IsMatch(fileContent[logIndex]), $"Not valid: '{fileContent[logIndex]}'");
        }
    }
}
