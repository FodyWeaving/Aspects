﻿
namespace CountersTests
{
    using Aspects.Counters;
    using NUnit.Framework;
    using System;
    using System.Diagnostics;


    public class CounterTestsWithNamesAsArgumentDeepNames
    {
        public class CounterDesciption
        {
            public string CounterCategory { get; set; } = "MyCategory";

            public string CounterName { get; set; } = "MyCounter";

            public string InstanceName { get; set; } = Guid.NewGuid().ToString();
        }

        private CounterDesciption Default = new CounterDesciption();

        [SetUp]
        public void Initialize()
        {
            if (!PerformanceCounterCategory.Exists(Default.CounterCategory)
                || !PerformanceCounterCategory.CounterExists(Default.CounterName, Default.CounterCategory))
            {
                PerformanceCounterCategory.Create(Default.CounterCategory, Default.CounterCategory,
                    PerformanceCounterCategoryType.MultiInstance, Default.CounterName, Default.CounterName);
            }
        }

        [TearDown]
        public void CleanUp()
        {
            if (!PerformanceCounterCategory.Exists(Default.CounterCategory)
               || !PerformanceCounterCategory.CounterExists(Default.CounterName, Default.CounterCategory))
            {
                return;
            }

            PerformanceCounterCategory.Delete(Default.CounterCategory);
        }

        [Test]
        public void Counters_WithNamesAsArgumentDeepNames_MethodShouldIncrementCounterValue()
        {
            using (PerformanceCounter counter
                    = new PerformanceCounter(Default.CounterCategory, Default.CounterName, Default.InstanceName, false))
            {
                counter.RawValue = 3;

                Increment(counter, Default);
                Decrement(counter, Default);
                IncrementByCount(counter, Default);

                counter.RawValue = 3;

                IncrementPercent(counter, Default);
                DecrementPercent(counter, Default);
            }
        }

        [IncrementCounter(CategoryName = "description.CounterCategory", CounterName = "description.CounterName", CounterInstanceName = "description.InstanceName", IncrementBy = 1)]
        private void Increment(PerformanceCounter counter, CounterDesciption description)
        {
            Assert.AreEqual(4, counter.RawValue);
        }

        [IncrementCounter(CategoryName = "description.CounterCategory", CounterName = "description.CounterName", CounterInstanceName = "description.InstanceName", IncrementBy = 3)]
        private void IncrementByCount(PerformanceCounter counter, CounterDesciption description)
        {
            Assert.AreEqual(6, counter.RawValue);
        }

        [DecrementCounter(CategoryName = "description.CounterCategory", CounterName = "description.CounterName", CounterInstanceName = "description.InstanceName")]
        private void Decrement(PerformanceCounter counter, CounterDesciption description)
        {
            Assert.AreEqual(3, counter.RawValue);
        }        
    
        [IncrementPercent(CategoryName = "description.CounterCategory", CounterName = "description.CounterName", CounterInstanceName = "description.InstanceName", Max = 10)]
        private void IncrementPercent(PerformanceCounter counter, CounterDesciption description)
        {
            Assert.AreEqual(13, counter.RawValue);
        }
    
        [DecrementPercent(CategoryName = "description.CounterCategory", CounterName = "description.CounterName", CounterInstanceName = "description.InstanceName", Max = 10)]
        private void DecrementPercent(PerformanceCounter counter, CounterDesciption description)
        {
            Assert.AreEqual(3, counter.RawValue);
        }
    }
}
