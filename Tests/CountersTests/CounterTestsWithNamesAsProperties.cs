﻿using Aspects.Counters;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountersTests
{
    public class CounterTestsWithNamesAsProperties
    {
        public long CounterValue
        {
            get;
            [Counter(CategoryName = "CounterCategory", CounterName = "CounterName", CounterInstanceName = "CustomInstanceName")]
            set;
        }

        public string CounterCategory
        {
            get { return "Custom aspect category"; }
        }

        public string CounterName
        {
            get
            {
                return "Custom counter name";
            }
        }

        public string CustomInstanceName
        {
            get
            {
                return "My custom instance";
            }
        }

        [SetUp]
        public void Initialize()
        {
            if (!PerformanceCounterCategory.Exists(CounterCategory)
                || !PerformanceCounterCategory.CounterExists(CounterName, CounterCategory))
            {
                PerformanceCounterCategory.Create(CounterCategory, CounterCategory,
                    PerformanceCounterCategoryType.MultiInstance, CounterName, CounterName);
            }

            using (PerformanceCounter counter
                = new PerformanceCounter(CounterCategory, CounterName, CustomInstanceName, false))
            {
                counter.RawValue = 3;
            }
        }

        [TearDown]
        public void CleanUp()
        {
            if (!PerformanceCounterCategory.Exists(CounterCategory)
               || !PerformanceCounterCategory.CounterExists(CounterName, CounterCategory))
            {
                return;
            }

            PerformanceCounterCategory.Delete(CounterCategory);
        }

        [Test]
        [IncrementCounter(CategoryName = "CounterCategory", CounterName = "CounterName", CounterInstanceName = "CustomInstanceName")]
        public void Counters_WithNamesAsProperties_MethodShouldIncrementCounterValue()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(CounterCategory, CounterName, CustomInstanceName, false))
            {
                Assert.AreEqual(4, counter.RawValue);
            }
        }

        [Test]
        [IncrementCounter(CategoryName = "CounterCategory", CounterName = "CounterName", IncrementBy = 3, CounterInstanceName = "CustomInstanceName")]
        public void Counters_WithNamesAsProperties__MethodShouldIncrementCounterValueBySpecifiedCount()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(CounterCategory, CounterName, CustomInstanceName, false))
            {
                Assert.AreEqual(6, counter.RawValue);
            }
        }

        [Test]
        [DecrementCounter(CategoryName = "CounterCategory", CounterName = "CounterName", CounterInstanceName = "CustomInstanceName")]
        public void Counters_WithNamesAsProperties__MethodShouldDecrementCounterValue()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(CounterCategory, CounterName, CustomInstanceName, false))
            {
                Assert.AreEqual(2, counter.RawValue);
            }
        }

        [Test]
        public void Counters_WithNamesAsProperties__PropertyShouldSetTheCounterValue()
        {
            Stopwatch watch = Stopwatch.StartNew();

            this.CounterValue = new Random().Next(0, Int32.MaxValue);

            Console.WriteLine($"Counter value modified after {watch.Elapsed}");

            using (PerformanceCounter counter
                = new PerformanceCounter(CounterCategory, CounterName, CustomInstanceName, false))
            {
                Assert.AreEqual(this.CounterValue, counter.RawValue);
            }
        }

        [Test]
        [IncrementPercent(CategoryName = "CounterCategory", CounterName = "CounterName", Max = 10, CounterInstanceName = "CustomInstanceName")]
        public void Counters_WithNamesAsProperties__MethodShouldIncrementPercent()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(CounterCategory, CounterName, CustomInstanceName, false))
            {
                Assert.AreEqual(13, counter.RawValue);
            }
        }

        [Test]
        [DecrementPercent(CategoryName = "CounterCategory", CounterName = "CounterName", Max = 50, CounterInstanceName = "CustomInstanceName")]
        public void Counters_WithNamesAsProperties__MethodShouldDecrementPercent()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(CounterCategory, CounterName, CustomInstanceName, false))
            {
                Assert.AreEqual(1, counter.RawValue);
            }
        }
    }
}
