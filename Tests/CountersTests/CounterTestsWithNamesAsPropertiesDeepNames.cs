﻿using Aspects.Counters;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountersTests
{
    public class CounterTestsWithNamesAsPropertiesDeepNames
    {
        public long CounterValue
        {
            get;
            [Counter(CategoryName = "Counter.CounterCategory", CounterName = "Counter.CounterName", CounterInstanceName = "Counter.CustomInstanceName")]
            set;
        }

        public class CounterDescription
        {
            public string CounterCategory
            {
                get { return "Custom aspect category"; }
            }

            public string CounterName
            {
                get
                {
                    return "Custom counter name";
                }
            }

            public string CustomInstanceName
            {
                get
                {
                    return "My custom instance";
                }
            }
        }

        public CounterDescription Counter { get; set; } = new CounterDescription();

        [SetUp]
        public void Initialize()
        {
            if (!PerformanceCounterCategory.Exists(Counter.CounterCategory)
                || !PerformanceCounterCategory.CounterExists(Counter.CounterName, Counter.CounterCategory))
            {
                PerformanceCounterCategory.Create(Counter.CounterCategory, Counter.CounterCategory,
                    PerformanceCounterCategoryType.MultiInstance, Counter.CounterName, Counter.CounterName);
            }

            using (PerformanceCounter counter
                = new PerformanceCounter(Counter.CounterCategory, Counter.CounterName, Counter.CustomInstanceName, false))
            {
                counter.RawValue = 3;
            }
        }

        [TearDown]
        public void CleanUp()
        {
            if (!PerformanceCounterCategory.Exists(Counter.CounterCategory)
               || !PerformanceCounterCategory.CounterExists(Counter.CounterName, Counter.CounterCategory))
            {
                return;
            }

            PerformanceCounterCategory.Delete(Counter.CounterCategory);
        }

        [Test]
        [IncrementCounter(CategoryName = "Counter.CounterCategory", CounterName = "Counter.CounterName", CounterInstanceName = "Counter.CustomInstanceName")]
        public void Counters_WithNamesAsPropertyDeepNames_MethodShouldIncrementCounterValue()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(Counter.CounterCategory, Counter.CounterName, Counter.CustomInstanceName, false))
            {
                Assert.AreEqual(4, counter.RawValue);
            }
        }

        [Test]
        [IncrementCounter(CategoryName = "Counter.CounterCategory", CounterName = "Counter.CounterName", IncrementBy = 3, CounterInstanceName = "Counter.CustomInstanceName")]
        public void Counters_WithNamesAsPropertyDeepNames__MethodShouldIncrementCounterValueBySpecifiedCount()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(Counter.CounterCategory, Counter.CounterName, Counter.CustomInstanceName, false))
            {
                Assert.AreEqual(6, counter.RawValue);
            }
        }

        [Test]
        [DecrementCounter(CategoryName = "Counter.CounterCategory", CounterName = "Counter.CounterName", CounterInstanceName = "Counter.CustomInstanceName")]
        public void Counters_WithNamesAsPropertyDeepNames__MethodShouldDecrementCounterValue()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(Counter.CounterCategory, Counter.CounterName, Counter.CustomInstanceName, false))
            {
                Assert.AreEqual(2, counter.RawValue);
            }
        }

        [Test]
        public void Counters_WithNamesAsPropertyDeepNames__PropertyShouldSetTheCounterValue()
        {
            Stopwatch watch = Stopwatch.StartNew();

            this.CounterValue = new Random().Next(0, Int32.MaxValue);

            Console.WriteLine($"Counter value modified after {watch.Elapsed}");

            using (PerformanceCounter counter
                = new PerformanceCounter(Counter.CounterCategory, Counter.CounterName, Counter.CustomInstanceName, false))
            {
                Assert.AreEqual(this.CounterValue, counter.RawValue);
            }
        }

        [Test]
        [IncrementPercent(CategoryName = "Counter.CounterCategory", CounterName = "Counter.CounterName", Max = 10, CounterInstanceName = "Counter.CustomInstanceName")]
        public void Counters_WithNamesAsPropertyDeepNames__MethodShouldIncrementPercent()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(Counter.CounterCategory, Counter.CounterName, Counter.CustomInstanceName, false))
            {
                Assert.AreEqual(13, counter.RawValue);
            }
        }

        [Test]
        [DecrementPercent(CategoryName = "Counter.CounterCategory", CounterName = "Counter.CounterName", Max = 50, CounterInstanceName = "Counter.CustomInstanceName")]
        public void Counters_WithNamesAsPropertyDeepNames__MethodShouldDecrementPercent()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(Counter.CounterCategory, Counter.CounterName, Counter.CustomInstanceName, false))
            {
                Assert.AreEqual(1, counter.RawValue);
            }
        }
    }
}
