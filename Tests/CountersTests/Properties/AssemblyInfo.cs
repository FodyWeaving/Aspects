using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[module: Aspects.Counters.Counter]
[module: Aspects.Counters.DecrementCounter]
[module: Aspects.Counters.DecrementPercent]
[module: Aspects.Counters.IncrementCounter]
[module: Aspects.Counters.IncrementPercent]
[module: Aspects.Counters.IncrementPerSeconds]

[assembly: AssemblyTitle("CountersTests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("CountersTests")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("36bc3edd-2dda-46af-8f61-cc8d551a99bf")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
