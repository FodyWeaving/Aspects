﻿
namespace CountersTests
{
    using Aspects.Counters;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    [TestFixture]
    public class AddCountersToCategoryTests
    {
        private const string Category = "Aspects-tests";

        protected PerformanceCounterType PerformanceCounterType { get; set; }

        [SetUp]
        public void Initialize()
        {
            if (PerformanceCounterCategory.Exists(Category))
            {
                PerformanceCounterCategory.Delete(Category);
            }
        }

        [TearDown]
        public void CleanUp()
        {
            if (!PerformanceCounterCategory.Exists(Category))
            {
                return;
            }

            PerformanceCounterCategory.Delete(Category);
        }

        [Test]
        public void TestAddMultipleCounters()
        {
            this.PerformanceCounterType = PerformanceCounterType.NumberOfItems64;
            var instanceName = Guid.NewGuid().ToString();

            this.CreateCounter("Counter1");

            var value = new Random().Next();

            using (PerformanceCounter counter
                    = new PerformanceCounter(Category, "Counter1", instanceName, false))
            {
                counter.RawValue = value;
            }

            this.CreateCounter("Counter2");

            using (PerformanceCounter counter
                   = new PerformanceCounter(Category, "Counter1", instanceName, false))
            {
                Assert.AreEqual(value, counter.RawValue);
            }
        }

        private void CreateCounter(string counter)
        {
            if (PerformanceCounterCategory.Exists(Category)
                && PerformanceCounterCategory.CounterExists(counter, Category))
            {
                return;
            }

            if (!PerformanceCounterCategory.Exists(Category))
            {
                PerformanceCounterCategory.Create(
                      categoryName: Category,
                      categoryHelp: Category,
                      categoryType: PerformanceCounterCategoryType.MultiInstance,
                      counterData: new CounterCreationDataCollection(new CounterCreationData[] {
                                            new CounterCreationData(counter, counter, this.PerformanceCounterType) }));

                return;
            }

            var category = new PerformanceCounterCategory(Category);

            // Get current counters
            var data = GetCountersData(category);

            var counterDataCollection = GetCreationCollection(category);

            // Add our counter
            counterDataCollection.Add(new CounterCreationData(counter, counter, this.PerformanceCounterType));

            // Remove current category
            PerformanceCounterCategory.Delete(Category);

            // Add all counters
            PerformanceCounterCategory.Create(
                   categoryName: Category,
                   categoryHelp: Category,
                   categoryType: PerformanceCounterCategoryType.MultiInstance,
                   counterData: counterDataCollection);

            // Resets counters data
            this.InitializeCountersData(category, data);
        }


        private IEnumerable<CounterInstanceValue> GetCountersData(PerformanceCounterCategory category)
        {
            var result = new List<CounterInstanceValue>();

            foreach (var instanceName in category.GetInstanceNames())
            {
                foreach (PerformanceCounter counter in category.GetCounters(instanceName))
                {
                    using (PerformanceCounter counterInstance = new PerformanceCounter(category.CategoryName, counter.CounterName, counter.InstanceName))
                    {
                        result.Add(new CounterInstanceValue
                        {
                            CounterName = counter.CounterName,
                            InstanceName = instanceName,
                            PerformanceCounterType = counter.CounterType,
                            Value = counterInstance.RawValue
                        });
                    }
                }
            }

            return result;
        }

        private CounterCreationDataCollection GetCreationCollection(PerformanceCounterCategory category)
        {
            var result = new CounterCreationDataCollection();

            foreach(var instanceName in category.GetInstanceNames())
            {
                foreach (var counter in category.GetCounters(instanceName))
                {
                    if (HasCounter(result, counter.CounterName))
                    {
                        continue;
                    }

                    result.Add(new CounterCreationData
                    {
                        CounterName = counter.CounterName,
                        CounterHelp = counter.CounterHelp,
                        CounterType = counter.CounterType
                    });
                }
            }          

            return result;
        }

        private bool HasCounter(CounterCreationDataCollection source, string counterName)
        {
            foreach(CounterCreationData item in source)
            {
                if (item.CounterName == counterName)
                    return true;
            }

            return false;
        }

        private void InitializeCountersData(PerformanceCounterCategory category, IEnumerable<CounterInstanceValue> data)
        {
            foreach (var item in data)
            {
                using (PerformanceCounter counterInstance = new PerformanceCounter(category.CategoryName, item.CounterName, item.InstanceName, false))
                {
                    counterInstance.RawValue = item.Value;
                }
            }
        }

        class CounterInstanceValue
        {
            public string CounterName { get; set; }

            public PerformanceCounterType PerformanceCounterType { get; set; }

            public string InstanceName { get; set; }

            public long Value { get; set; }
        }
    }
}
