﻿
namespace CountersTests
{
    using Aspects.Counters;
    using NUnit.Framework;
    using System;
    using System.Diagnostics;


    public class CounterTestsWithNamesAsArguments
    {
        private string CounterCategory = "MyCategory";

        private string CounterName = "MyCounter";

        [SetUp]
        public void Initialize()
        {
            if (!PerformanceCounterCategory.Exists(CounterCategory)
                || !PerformanceCounterCategory.CounterExists(CounterName, CounterCategory))
            {
                PerformanceCounterCategory.Create(CounterCategory, CounterCategory,
                    PerformanceCounterCategoryType.MultiInstance, CounterName, CounterName);
            }
        }

        [TearDown]
        public void CleanUp()
        {
            if (!PerformanceCounterCategory.Exists(CounterCategory)
               || !PerformanceCounterCategory.CounterExists(CounterName, CounterCategory))
            {
                return;
            }

            PerformanceCounterCategory.Delete(CounterCategory);
        }

        [Test]
        public void Counters_WithNamesAsArguments_MethodShouldIncrementCounterValue()
        {
            var instanceName = Guid.NewGuid().ToString();

            using (PerformanceCounter counter
                    = new PerformanceCounter(CounterCategory, CounterName, instanceName, false))
            {
                counter.RawValue = 3;

                Increment(counter, CounterCategory, CounterName, instanceName);
                Decrement(counter, CounterCategory, CounterName, instanceName);
                IncrementByCount(counter, CounterCategory, CounterName, instanceName);

                counter.RawValue = 3;

                IncrementPercent(counter, CounterCategory, CounterName, instanceName);
                DecrementPercent(counter, CounterCategory, CounterName, instanceName);
            }
        }

        [IncrementCounter(CategoryName = "category", CounterName = "counterName", CounterInstanceName = "instanceName", IncrementBy = 1)]
        private void Increment(PerformanceCounter counter, string category, string counterName, string instanceName)
        {
            Assert.AreEqual(4, counter.RawValue);
        }

        [IncrementCounter(CategoryName = "category", CounterName = "counterName", CounterInstanceName = "instanceName", IncrementBy = 3)]
        private void IncrementByCount(PerformanceCounter counter, string category, string counterName, string instanceName)
        {
            Assert.AreEqual(6, counter.RawValue);
        }

        [DecrementCounter(CategoryName = "category", CounterName = "counterName", CounterInstanceName = "instanceName")]
        private void Decrement(PerformanceCounter counter, string category, string counterName, string instanceName)
        {
            Assert.AreEqual(3, counter.RawValue);
        }        
    
        [IncrementPercent(CategoryName = "category", CounterName = "counterName", CounterInstanceName = "instanceName", Max = 10)]
        private void IncrementPercent(PerformanceCounter counter, string category, string counterName, string instanceName)
        {
            Assert.AreEqual(13, counter.RawValue);
        }
    
        [DecrementPercent(CategoryName = "category", CounterName = "counterName", CounterInstanceName = "instanceName", Max = 10)]
        private void DecrementPercent(PerformanceCounter counter, string category, string counterName, string instanceName)
        {
            Assert.AreEqual(3, counter.RawValue);
        }
    }
}
