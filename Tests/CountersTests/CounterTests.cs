﻿
namespace CountersTests
{
    using Aspects.Counters;
    using NUnit.Framework;
    using System;
    using System.Diagnostics;

    [TestFixture]
    public class CounterTests
    {
        private const string CounterCategory = "Aspects-tests";

        private const string CounterName = "Counter tests";

        public long CounterValue
        {
            get;
            [Counter(CategoryName = CounterCategory, CounterName = CounterName)]
            set;
        }

        public string CustomCategory = "Custom aspect category";

        public string CustomCounterName = "Custom counter name";

        public string CustomInstanceName = "My custom instance";
        
        [SetUp]
        public void Initialize()
        {
            if(!PerformanceCounterCategory.Exists(CounterCategory)
                || !PerformanceCounterCategory.CounterExists(CounterName, CounterCategory))
            {
                PerformanceCounterCategory.Create(CounterCategory, CounterCategory, PerformanceCounterCategoryType.MultiInstance, CounterName, CounterName);
            }

            using (PerformanceCounter counter
                = new PerformanceCounter(CounterCategory, CounterName, Process.GetCurrentProcess().ProcessName, false))
            {
                counter.RawValue = 3;
            }
        }

        [TearDown]
        public void CleanUp()
        {
            if (!PerformanceCounterCategory.Exists(CounterCategory)
               || !PerformanceCounterCategory.CounterExists(CounterName, CounterCategory))
            {
                return;
            }

            PerformanceCounterCategory.Delete(CounterCategory);
        }

        [Test]
        [IncrementCounter(CategoryName = CounterCategory, CounterName = CounterName)]
        public void Counters_MethodShouldIncrementCounterValue()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(CounterCategory, CounterName, Process.GetCurrentProcess().ProcessName, false))
            {
                Assert.AreEqual(4, counter.RawValue);
            }
        }

        [Test]
        [IncrementCounter(CategoryName = CounterCategory, CounterName = CounterName, IncrementBy = 3)]
        public void Counters_MethodShouldIncrementCounterValueBySpecifiedCount()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(CounterCategory, CounterName, Process.GetCurrentProcess().ProcessName, false))
            {
                Assert.AreEqual(6, counter.RawValue);
            }
        }

        [Test]
        [DecrementCounter(CategoryName = CounterCategory, CounterName = CounterName)]
        public void Counters_MethodShouldDecrementCounterValue()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(CounterCategory, CounterName, Process.GetCurrentProcess().ProcessName, false))
            {
                Assert.AreEqual(2, counter.RawValue);
            }
        }

        [Test]
        public void Counters_PropertyShouldSetTheCounterValue()
        {
            Stopwatch watch = Stopwatch.StartNew();

            this.CounterValue = new Random().Next(0, Int32.MaxValue);

            Console.WriteLine($"Counter value modified after {watch.Elapsed}");

            using (PerformanceCounter counter
                = new PerformanceCounter(CounterCategory, CounterName, Process.GetCurrentProcess().ProcessName, false))
            {
                Assert.AreEqual(this.CounterValue, counter.RawValue);
            }
        }

        [Test]
        [IncrementPercent(CategoryName = CounterCategory, CounterName = CounterName, Max = 10)]
        public void Counters_MethodShouldIncrementPercent()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(CounterCategory, CounterName, Process.GetCurrentProcess().ProcessName, false))
            {
                Assert.AreEqual(13, counter.RawValue);
            }
        }

        [Test]
        [DecrementPercent(CategoryName = CounterCategory, CounterName = CounterName, Max = 50)]
        public void Counters_MethodShouldDecrementPercent()
        {
            using (PerformanceCounter counter
                = new PerformanceCounter(CounterCategory, CounterName, Process.GetCurrentProcess().ProcessName, false))
            {
                Assert.AreEqual(1, counter.RawValue);
            }
        }

    }
}
