[![build status](https://gitlab.com/FodyWeaving/Aspects/badges/master/build.svg)](https://gitlab.com/FodyWeaving/Aspects/commits/master)

# What is Aspects ?
Aspects is a AOP tool based on Fody, that allow you to encapsulate your code with attributes.

## How to use it ?

### Install the Aspect.Fody package to your library

```sh
PM> Install-Package Aspects.Fody
```

# Ready to use aspects
## Logging
Aspects that simplfye logging features with logging context. (with the powerfull log4net)
```
PM> Install-Package Weaving.Aspects.Logging
```

### Example

```cs
        [StartTask(Message = "Test method called")]
        public void TestMethod()
        {
            var result = SubMethod1();

            SubMethod2(result);
        }

        [Debug]
        [Context("ContextKey", "Context value")]
        private string SubMethod1()
        {
            return "My return statement";
        }

        [Debug]
        private void SubMethod2(string arg)
        {

        }
```
Will generate these logs:

```txt
2017-08-17 21:26:31,523 [1] INFO  ConsoleApp2.TestClass [e544bd89-1d66-49a7-8715-b720bea6634e] - Test method called
2017-08-17 21:26:31,560 [1] DEBUG ConsoleApp2.TestClass [e544bd89-1d66-49a7-8715-b720bea6634e] - SubMethod1 called with id 24708441-7583-4d79-af62-1bb2ab0bb495 and arguments: .
2017-08-17 21:26:31,565 [1] DEBUG ConsoleApp2.TestClass [e544bd89-1d66-49a7-8715-b720bea6634e] - SubMethod1 with id 24708441-7583-4d79-af62-1bb2ab0bb495 returns 'My return statement' (System.String).
2017-08-17 21:26:31,570 [1] DEBUG ConsoleApp2.TestClass [e544bd89-1d66-49a7-8715-b720bea6634e] - SubMethod2 called with id 1b5fa105-2820-43e0-bf4c-8a4c362023c2 and arguments: arg 'My return statement' (System.String).
2017-08-17 21:26:31,574 [1] DEBUG ConsoleApp2.TestClass [e544bd89-1d66-49a7-8715-b720bea6634e] - SubMethod2 with id 1b5fa105-2820-43e0-bf4c-8a4c362023c2 returns void.
```

More informations at [Wiki pages](https://gitlab.com/FodyWeaving/Aspects/wikis/Instrumentation).

## Performance counters
Aspects that help to integrate Performance counter manipulation
```
PM> Install-Package Weaving.Aspects.Counters
```

More informations at [Wiki pages](https://gitlab.com/FodyWeaving/Aspects/wikis/Performance%20counters).

# How-to write your aspect ?
```cs
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor)]
    public class MyAspectAttribute : Attribute, IAspect
    {
        public string StringParameter { get; set; }

        public int NumericParameter { get; set; }

        public void Init(object instance, MethodBase method, string[] argNames, object[] args)
        {
            throw new NotImplementedException();
        }

        public void OnEntry()
        {
            throw new NotImplementedException();
        }

        public void OnException(Exception exception)
        {
            throw new NotImplementedException();
        }

        public void OnExit(object result)
        {
            throw new NotImplementedException();
        }
    }
```

Add your attribute to your methods
```cs
	   public int TestValue
       {
			get; [MyAspect]set;
       }

       [MyAspect(StringParameter = "AspectParameterValue")]
       public void MyMethod()
       {
           ...
       }
```

### Modify your FodyWeavers.xml file
```xml
<?xml version="1.0" encoding="utf-8" ?>
<Weavers xmlns="http://tempuri.org/Aspects.xsd">
  <Aspects>
	 <AspectReferences>
      <Assembly FullName="AssemblyToProcess, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null">
        <Aspect Order="100" Name="MyAspect" Reference="AssemblyToProcess.MyAspectAttribute" />
        <Aspect Order="200" Name="MyOtherAspect" Reference="AssemblyToProcess.MyAspectAttribute" />
      </Assembly>
    </AspectReferences>
  </Aspects>
</Weavers>
```

## What gets compiled ? 

```cs
    
	public int TestValue
		{
			[CompilerGenerated]
			get
			{
				return this.<TestValue>k__BackingField;
			}
			[MyAspect, CompilerGenerated]
			set
			{
				string[] array = new string[1];
				object[] array2 = new object[1];
				array[0] = "value";
				array2[0] = value;
				MethodBase methodFromHandle = MethodBase.GetMethodFromHandle(methodof(Class1.set_TestValue(int)).MethodHandle, typeof(Class1).TypeHandle);
				AspectAttribute aspectAttribute = (AspectAttribute)Activator.CreateInstance(typeof(AspectAttribute));
				aspectAttribute.Init(this, methodFromHandle, array, array2);
				try
				{
					aspectAttribute.OnEntry();
					this.<TestValue>k__BackingField = value;
				}
				catch (Exception exception)
				{
					aspectAttribute.OnException(exception);
					throw;
				}
				aspectAttribute.OnExit(null);
			}
		}

	[MyAspect(StringParameter = "AspectParameterValue")]
	public void MyMethod()
	{
		MethodBase methodFromHandle = MethodBase.GetMethodFromHandle(methodof(MyClass.MyMethod()).MethodHandle, typeof(MyClass).TypeHandle);
		LibraryToWeave.Aspects.FirstAspect myAspect = (MyAspect)Activator.CreateInstance(typeof(MyAspect));
		myAspect.StringParameter = "AspectParameterValue";
		myAspect.Init(this, methodFromHandle, null, null);
		try
		{
			myAspect.OnEntry();

			...
		}
		catch (Exception exception)
		{
			myAspect.OnException(exception);
			throw;
		}

		myAspect.OnExit(null);
		return;
	}
```

## How to inject aspects ?
Modify your FordyWeavers.xml file like this: 

```xml
<?xml version="1.0" encoding="utf-8" ?>
<Weavers xmlns="http://tempuri.org/Aspects.xsd">
  <Aspects>
    <AspectReferences>
     <Assembly FullName="AssemblyToProcess, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null">
        <Aspect Order="100" Name="AspectWithProperties" Reference="AssemblyToProcess.AspectWithPropertiesAttribute">
          <Parameters>
            <Parameter Name="StringParameter" Value="Global parameter" />
          </Parameters>
        </Aspect>
      </Assembly>
    </AspectReferences>
    <MethodReferences>
      <Method Reference="System.Void AssemblyToProcess.InjectedClass::TestWithNoResult()">
        <Inject Name="AspectWithProperties">
          <Parameters>
            <Parameter Name="StringParameter" Value="Method parameter" />
            <Parameter Name="NumericParameter" Value="10" />
          </Parameters>
        </Inject>
      </Method>
      <Method Reference="System.Int32 AssemblyToProcess.InjectedClass::TestWithResult()">
        <Inject Name="AspectWithProperties">
          <Parameters>
            <Parameter Name="NumericParameter" Value="100" />
          </Parameters>
        </Inject>
      </Method>
    </MethodReferences>
  </Aspects>
</Weavers>
```

